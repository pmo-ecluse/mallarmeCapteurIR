#include "ContourFinderOsc.h"

ContourFinderOsc::ContourFinderOsc() :
    ofxCv::ContourFinder(),
    ofxOscRouterNode("contourFinder")
{
    addOscMethod("threshold");
    addOscMethod("captureBlack");
    addOscMethod("sortBySize");
    addOscMethod("areaMin");
    addOscMethod("areaMax");
    addOscMethod("trackerPersistence");
    addOscMethod("trackerDistance");
}

ContourFinderOsc::~ContourFinderOsc(){}

void ContourFinderOsc::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "threshold"))
    {
        if(validateOscSignature("([if])", m))
        {
            setThreshold(ofClamp(getArgAsFloatUnchecked(m, 0), 0, 255));
        }
    }
    else if(isMatch(command, "captureBlack"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            setInvert(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "sortBySize"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            setSortBySize(getArgAsBoolUnchecked(m, 0));
        }
    }
    else if(isMatch(command, "areaMin"))
    {
        if(validateOscSignature("([if])", m))
        {
            setMinAreaRadius(ofClamp(getArgAsFloatUnchecked(m, 0), 0, 100));
        }
    }
    else if(isMatch(command, "areaMax"))
    {
        if(validateOscSignature("([if])", m))
        {
            setMaxAreaRadius(ofClamp(getArgAsFloatUnchecked(m, 0), 0, 300));
        }
    }
    else if(isMatch(command, "trackerPersistence"))
    {
        if(validateOscSignature("([if])", m))
        {
            tracker.setPersistence((unsigned int) ofClamp(getArgAsIntUnchecked(m, 0), 0, 100));
        }
    }
    else if(isMatch(command, "trackerDistance"))
    {
        if(validateOscSignature("([if])", m))
        {
            tracker.setMaximumDistance(getArgAsFloatUnchecked(m, 0));
        }
    }
}
