#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "ofxOsc.h"
#include "ofxOscRouter.h"

#include "Camera.h"
#include "ContourFinderOsc.h"

class Core : public ofBaseApp, public ofxOscRouter
{
public:
    Core() :
        ofBaseApp(),
        ofxOscRouter("mallarmeIR", 9876)
    {
        ofxOscRouter::addOscMethod("exit");
        ofxOscRouter::addOscMethod("afficherImageCamera");
        ofxOscRouter::addOscMethod("afficherContours");
        ofxOscRouter::addOscMethod("afficherTrackers");
    };

    void setup();
    void update();
    void draw();

    void processOscCommand(const string& command, const ofxOscMessage& m);

    void keyPressed(int key);
    void mouseMoved(int x, int y);

    bool bAfficherImageCamera;
    bool bAfficherContours;
    bool bAfficherTrackers;
    Camera cam;
    cv::Mat frame;
    ContourFinderOsc contourFinder;
    ofxOscMessage oscMessage;
    ofxOscSender oscSend;
};
