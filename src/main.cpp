#include <memory>
#include "ofMain.h"
#include "Core.h"

//========================================================================
int main()
{
#ifdef DEBUG
    ofSetLogLevel(OF_LOG_VERBOSE);
    ofSetLogLevel("mallarmeIR", OF_LOG_VERBOSE);
    ofSetEscapeQuitsApp(true);
#else
    ofSetLogLevel(OF_LOG_SILENT);
    ofSetLogLevel("mallarmeIR", OF_LOG_NOTICE);
    ofSetEscapeQuitsApp(true);
#endif // DEBUG
    Core * captureApplication = new Core();
    ofSetupOpenGL(800, 600, OF_WINDOW);
    ofRunApp(captureApplication);
    return 0;
}

