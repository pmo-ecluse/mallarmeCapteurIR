#include "Core.h"

void Core::setup()
{
    bAfficherImageCamera = false;
    bAfficherContours = false;
    bAfficherTrackers = false;
    addOscChild(&cam);
    addOscChild(&contourFinder);
    cam.setup(800, 600, false);
    cam.setShutterSpeed(330000);
    cam.setExposureMode((MMAL_PARAM_EXPOSUREMODE_T) MMAL_PARAM_EXPOSUREMODE_FIXEDFPS);
    cam.setAWBMode((MMAL_PARAM_AWBMODE_T) MMAL_PARAM_AWBMODE_INCANDESCENT);
    contourFinder.setMinAreaRadius(20);
    contourFinder.setMaxAreaRadius(100);
    contourFinder.getTracker().setPersistence(60);
    contourFinder.getTracker().setMaximumDistance(160.);
    oscSend.setup("10.0.1.100", 9876);
}

void Core::update()
{
    ofxOscRouter::update();
    frame = cam.grab();

    if(!frame.empty())
    {
        contourFinder.findContours(frame);
    }
}

void Core::draw()
{
    ofBackground(0);
    ofSetColor(255);

    if(!frame.empty())
    {
        if(bAfficherImageCamera)
        {
            ofxCv::drawMat(frame, 0, 0);
        }

        if(bAfficherContours)
        {
            for(unsigned int i = 0; i < contourFinder.size(); ++i)
            {
                ofSetColor(0, 255, 0, 200);
                contourFinder.getPolyline(i).draw();
                ofSetColor(255, 0, 0, 200);
                ofCircle(ofxCv::toOf(contourFinder.getCentroid(i)), 30);

                if(bAfficherTrackers)
                {
                    ofDrawBitmapStringHighlight(ofToString(contourFinder.getLabel(i)), ofxCv::toOf(contourFinder.getCentroid(i)));
                }
            }
        }

        if(contourFinder.size() > 0)
        {
            ofVec2f position = ofxCv::toOf(contourFinder.getCentroid(0));
            oscMessage.clear();
            oscMessage.setAddress("/mallarme3D/annabelle/position");
            oscMessage.addFloatArg(position.x / cam.width);
            oscMessage.addFloatArg(position.y / cam.height);
            oscSend.sendMessage(oscMessage);

            if(contourFinder.size() > 1)
            {
                ofVec2f position = ofxCv::toOf(contourFinder.getCentroid(1));
                oscMessage.clear();
                oscMessage.setAddress("/mallarme3D/florence/position");
                oscMessage.addFloatArg(position.x / cam.width);
                oscMessage.addFloatArg(position.y / cam.height);
                oscSend.sendMessage(oscMessage);
            }
        }
    }

    ofSetColor(255, 0, 0);
    ofDrawBitmapString(ofToString((int) ofGetFrameRate()) + " fps", 10, 10);
}

void Core::processOscCommand(const string & command, const ofxOscMessage & m)
{
    if(isMatch(command, "exit"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            if(getArgAsBoolUnchecked(m, 0))
            {
                ofExit();
            }
        }
    }
    else if(isMatch(command, "afficherImageCamera"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bAfficherImageCamera = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "afficherContours"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bAfficherContours = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "afficherTrackers"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            bAfficherTrackers = getArgAsBoolUnchecked(m, 0);
        }
    }
}
void Core::keyPressed(int key)
{
    if(key == ' ')
    {
    }
}
void Core::mouseMoved(int x, int y)
{
    cam.setBrightness(ofMap(x, 0, ofGetWidth(), 0, 100));
    cam.setContrast(ofMap(y, 0, ofGetHeight(), -100, 100));
}
